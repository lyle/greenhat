# System Helper
module GreenHat
  # Log Manager
  class Log
    include LogFormat

    attr_accessor :thing, :socket

    # Check to see if it should be loaded
    def initialize(thing)
      self.thing = thing

      if SuperLog.type? name
        process
      else
        puts "--- Not Logs: #{name}"
      end
    end

    # Send to ElK and add to log directory
    def process
      elk
    end

    # ---------------------------
    # Quick Access
    def file
      thing.file
    end

    def name
      thing.name
    end

    # Ever Use host name?
    def host
      thing.name
    end

    def build_path(args = nil)
      thing.build_path(args)
    end

    def log_dir
      Sauce.settings.log_dir
    end
    # ---------------------------

    def mia
      puts 'Log: MIA:' + name
    end

    # Ship the Logs to ELK
    def elk
      if SuperLog.type?(name)
        send(SuperLog.type?(name))
      else
        puts "Log Format missing: #{name}"
        nil
      end
      socket_close
    end

    def post(row)
      loop do
        self.socket = TCPSocket.new(HOST, 5000) if socket.nil?
        socket.puts Oj.dump(row)
        break
      rescue StandardError => e
        # puts "Logtash - #{e.message}"
        # Wait and Retry
        sleep 2
        self.socket = nil
      end
    end

    def socket_close
      return true unless socket

      socket.close
      self.socket = nil
    end
  end
end
