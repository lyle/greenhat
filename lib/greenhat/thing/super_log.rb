# rubocop:disable  Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity, Metrics/LineLength
module GreenHat
  # Log Identifier
  module SuperLog
    def self.type?(name)
      return :raw_format if %w[
        nginx_gitlab_access_log
        nginx_gitlab_pages_access_log
        gitlab_rails_application_log
        unicorn_unicorn_stdout_log
      ].any? { |x| name.include? x }

      # return :sidekiq_format if %w[
      #   sidekiq_log
      #   sidekiq_current
      # ].any? { |x| name.include? x }

      return :api_json_format if %w[
        rails_api_json_log
      ].any? { |x| name.include? x }

      return :unicorn_stderr_format if %w[
        unicorn_stderr
      ].any? { |x| name.include? x }

      return :reconfigure_format if %w[
        reconfigure
      ].any? { |x| name.include? x }

      return :shellwords if %w[
        gitlab_pages_current
        alertmanager_current
        registry_current
        prometheus_current
        gitlab_shell_gitlab_shell_log
      ].any? { |x| name.include? x }

      return :time_space if %w[
        postgresql_current
        redis_current
        unicorn_current
        gitlab_workhorse_current
        gitlab_monitor_current
        sidekiq_exporter_log
      ].any? { |x| name.include? x }

      return :json_format if %w[
        production_json_log
        gitaly_current
        geo_log
        sidekiq_current
        sidekiq_log
      ].any? { |x| name.include? x }

      return :mia if %w[
        rails_production_log
        gitlab_rails_audit_json_log
      ].any? { |x| name.include? x }

      return :dmesg_format if %w[
        dmesg
      ].any? { |x| name.include? x }

      return :test_format if %w[
        asdfasdfasdf
      ].any? { |x| name.include? x }

      # These break
      # gitlab_rails_geo_log
      nil
    end
  end
end
# rubocop:enable  Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity, Metrics/LineLength
