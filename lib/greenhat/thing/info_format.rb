module GreenHat
  # Log Helper
  module InfoFormat
    def format_df_h(data)
      {
        header: data.first.split(' ', 6),
        rows: split_white_space(data[1..-1]).sort_by { |x| x[-2].to_i }.reverse
      }
    end

    def format_mount(data)
      data.select { |x| x.include? ':' }
    end

    def format_free_m(data)
      split_white_space(data)
    end

    def format_meminfo(data)
      data.map { |x| x.split(' ', 2) }
    end

    def format_netstat(data)
      # Data can return blank or single item array
      entries = data[2..-1]
      return nil if entries.nil?

      headers = [
        'Proto', 'Local Address', 'Foreign Address', 'State', 'PID/Program name'
      ]

      {
        headers: headers,
        rows: entries.map { |x| x.split(' ', 7).reject { |y| y == '0' } }
      }
    end

    def split_white_space(data)
      data.map { |row| row.split(' ') }
    end

    def ps_format
      headers = info.ps.first.split(' ', 11)
      list = info.ps[1..-1].each.map do |row|
        row.split(' ', 11).each_with_index.each_with_object({}) do |(v, i), obj|
          obj[headers[i]] = v
        end
      end
      { headers: headers, list: list }
    end

    def manifest_json_format
      Oj.load info[:gitlab_version_manifest_json].join
    end

    def cpuinfo_format
      info.cpuinfo.join("\n").split("\n\n").map do |cpu|
        all = cpu.split("\n").map do |row|
          row.delete("\t").split(': ')
        end
        { details: all[1..-1], order: all[0].last }
      end
    end

    def cpu_speed
      return nil unless data? :lscpu

      info.lscpu.find { |x| x.include? 'MHz' }.split('               ')
    end

    def total_memory
      return nil unless data? :free_m

      value = info.free_m.dig(1, 1).to_i
      number_to_human_size(value * 1024 * 1024)
    end

    def ulimit
      return nil unless data? :ulimit

      results = info.ulimit.map do |entry|
        {
          value: entry.split(' ')[-1],
          details: entry.split('  ').first
        }
      end

      results.sort_by { |x| x[:details].downcase }
    end

    def systemctl_format
      return nil unless data? :systemctl_unit_files

      all = info.systemctl_unit_files[1..-2].map do |x|
        unit, status = x.split(' ')
        { unit: unit, status: status }
      end
      all.reject! { |x| x[:unit].nil? }
      all.sort_by(&:unit)
    end

    # Helper to color the status files
    def systemctl_color(entry)
      case entry.status
      when 'enabled'  then :green
      when 'static'   then :orange
      when 'disabled' then :red
      else
        :grey
      end
    end

    def vmstat_format
      return nil unless data? :vmstat

      info.vmstat[2..-1].map { |x| x.split(' ') }
    end

    def uptime
      info.uptime.join.split(',', 4).map(&:lstrip) if info.key? :uptime
    end

    def mount
      return nil unless info.key? :mount
      return nil if info.mount.empty?

      MountFormat.parse(info.mount)
    end
  end
end
