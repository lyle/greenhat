# System Helper
module GreenHat
  # TODO: This is pretty messy, Refactor
  # rubocop:disable Metrics/ModuleLength, Metrics/AbcSize, Metrics/LineLength, Style/NumericPredicate
  # Log Helper
  module LogFormat
    def path_format
      build_path('/')
    end

    def raw_format
      IO.foreach(file) do |row|
        post(time: datetime, source: path_format, msg: row, host: thing.host)
      end
    end

    def sidekiq_format
      IO.foreach(file) do |row|
        time, _time, pid, thread, msg = row.split(' ', 5)

        result = {
          time: parse_datetime(time), pid: pid,
          thread: thread, msg: msg, source: path_format, host: thing.host
        }

        # Calculate Duration
        result[:duration] = msg[/done:(.*?)sec/m, 1].to_f if msg&.include?('done:')

        post result
      end
    end

    def unicorn_stderr_format
      IO.foreach(file) do |row|
        details = row.split(' ', 7)[1..-1]
        level = details[2]
        time = parse_datetime(details.first)
        msg = details.last
        post(
          time: time, level: level, msg: msg,
          source: path_format, host: thing.host
        )
      rescue StandardError => e
        puts "Unable to Parse, #{name}:#{e.message}"
        post(time: datetime, msg: row, source: path_format, host: thing.host)
      end
    end

    def reconfigure_format
      IO.foreach(file) do |row|
        time, level, msg = row.split(' ', 3)
        post(
          time: parse_datetime(time),
          level: level.delete(':'),
          msg: msg,
          host: thing.host,
          source: path_format
        )
      rescue StandardError => e
        puts "Unable to Parse, #{name}:#{e.message}"
        post(time: datetime, msg: row, source: path_format, host: thing.host)
      end
    end

    def shellwords
      IO.foreach(file) do |row|
        result = Shellwords.split(row).each_with_object({}) do |x, h|
          key, value = x.split('=')
          next if value.nil?

          h[key] = value.numeric? ? value.to_f : value
          h[key] = 0.0 if h[key] == 0
        end
        result['host'] = thing.host
        result['source'] = path_format
        result['time'] = format_time(result)

        post result
      end
    end

    def test_format
      # rubocop:disable Lint/Debugger
      binding.pry
      # rubocop:enable Lint/Debugger
    end

    def dmesg_format
      IO.foreach(file) do |row|
        next if row.empty? || row == "\n"

        result = {}
        time, data = row.split(']', 2)
        result['stamp'] = time.split('[', 2).last.strip

        if data.include? ': '
          category, raw = data.split(': ', 2)
          result['category'] = category.strip

          result.merge! dmesg_split(raw) if raw.include? '='

          result['message'] = raw.strip
        else
          result['message'] = data
        end

        result['host'] = thing.host
        result['source'] = path_format
        result['time'] = format_time(result)

        post result
      rescue StandardError => e
        puts "Unable to Parse, #{row}:#{e.message}"
      end
    end

    def dmesg_split(raw)
      Shellwords.split(raw).each_with_object({}) do |x, h|
        key, value = x.split('=')
        next if value.nil?

        h[key] = value.numeric? ? value.to_f : value
        h[key] = 0.0 if h[key] == 0
      end
    end

    def format_time(result)
      if result.key? 'time'
        parse_datetime(result['time'])
      elsif result.key? 'ts'
        parse_datetime(result['ts'])
      else
        datetime
      end
    end

    def datetime
      Time.now.strftime('%Y-%m-%dT%H:%M:%S.%L%z')
    end

    def parse_datetime(time)
      Time.parse(time).strftime('%Y-%m-%dT%H:%M:%S.%L%z')
    rescue StandardError
      datetime
    end

    def json_format
      # binding.pry if path_format.include? 'sidekiq'
      IO.foreach(file) do |row|
        result = Oj.load(row)
        result['source'] = path_format
        result.delete('params') # Breaks index

        # Breaks index sidekiq Integer/Boolean
        result['retry'] = result['retry'] ? true : false
        # result.delete('retry') if result['retry'].class == Integer

        result['time'] = format_time(result)
        result['host'] = thing.host
        post result
      rescue StandardError => e
        puts "Unable to Parse, #{name}:#{e.message}"
        post(time: datetime, msg: row, source: path_format, host: thing.host)
      end
    end

    def api_json_format
      IO.foreach(file) do |row|
        result = Oj.load(row)
        result['source'] = path_format
        result['time'] = format_time(result)
        result['host'] = thing.host
        result['db'] = 0.0 if result['db'] == 0
        result.delete('params') # Breaks index
        post result
      rescue StandardError => e
        puts "Unable to Parse, #{name}:#{e.message}"
        post(ime: datetime, msg: row, source: path_format, host: thing.host)
      end
    end

    def time_space
      IO.foreach(file) do |row|
        time, msg = row.split(' ', 2)
        result = {
          time: parse_datetime(time), msg: msg, host: thing.host,
          source: path_format
        }

        result[:time] ||= datetime
        post result
      rescue StandardError => e
        puts "Unable to Parse, #{name}:#{e.message}"
        post(time: datetime, msg: row, source: path_format, host: thing.host)
      end
    end
  end
end

# rubocop:enable Metrics/ModuleLength, Metrics/AbcSize, Metrics/LineLength, Style/NumericPredicate

# Pretty sure this is a bad idea to patch string.
# https://mentalized.net/journal/2011/04/14/ruby-how-to-check-if-a-string-is-numeric/
class String
  def numeric?
    !Float(self).nil?
  rescue StandardError
    false
  end
end

class TrueClass
  def to_i
    1
                 end; end
class FalseClass
  def to_i
    0
                  end; end
