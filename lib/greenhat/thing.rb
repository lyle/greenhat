# System Helper
module GreenHat
  # File Manipulator
  class Thing
    include InfoFormat

    attr_accessor :path, :base_dir, :size, :name, :raw, :file, :archive
    def initialize(file, base_dir, archive)
      self.archive = archive
      self.file = file
      self.base_dir = base_dir
      self.name = build_path.gsub(/[^\w\s]/, '_')
      self.size = File.size(file) / (1024 * 1024)
      spin
      time = Benchmark.realtime { process }
      # output = process
      # binding.pry
      done time.round(2)
    end

    def color(*splat)
      puts Pastel.new.decorate(*splat)
    end

    def done(time)
      @spinner.update(title: "Done(#{time}s)")
      @spinner.success
    end

    def spin
      text = "[:spinner] :title #{name}: #{size}MB / #{type}"
      @spinner = TTY::Spinner.new(text, hide_cursor: true)
      @spinner.update(title: 'Loading')
      @spinner.auto_spin
    end

    def type
      if info?
        :info
      elsif log?
        :log
      else
        :raw
      end
    end

    # Ever Use host name?
    def host
      name
    end

    def info?
      methods.include? "format_#{name}".to_sym
    end

    def info
      self.raw = send "format_#{name}", read_raw
    end

    def log?
      if SuperLog.type? name
        true
      else
        false
      end
    end

    # No formatting, not a large log file
    def read_raw
      self.raw = File.read(file).split("\n")
    end

    # Read and Process
    def process
      case type
      when :info then info
      when :log then Log.new(self)
      else
        return false if size >= 5

        read_raw
      end
    end

    # Format the name of this thing
    def build_path(divider = '_')
      tmp_path = file.gsub(base_dir + '/', '')

      case tmp_path.count('/')
      when 0
        tmp_path
      when 1
        tmp_path.split('/').last
      else
        tmp_path.split('/').last(2).join(divider)
      end
    end
  end
end
