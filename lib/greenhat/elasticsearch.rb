# Top level namespace
module GreenHat
  # Elasticsearch Status
  module Elasticsearch
    def self.color(detail, color = :bright_blue)
      Pastel.new.decorate(detail, color)
    end

    def self.spin
      @spinner = TTY::Spinner.new('[:spinner] :title', hide_cursor: true)
      @spinner.update(title: 'Waiting for Elasticsearch')
      @spinner.auto_spin
    end

    def self.done
      @spinner.update(title: color('Elasticsearch Ready', :green))
      @spinner.success
    end

    def self.status
      spin
      loop do
        sleep 5
        break if query == 'green'
      end
      done
    end

    def self.query
      HTTParty.get('http://127.0.0.1:9200/_cluster/health')&.status
    # This can loop forever, but output sucks
    rescue StandardError
      nil
    end
  end
end
