# Top level namespace
module GreenHat
  # Archive Manipulator
  module Archive
    def self.load(archive)
      path = "#{$TMP}/#{SecureRandom.uuid}"
      Dir.mkdir path

      if archive.include? '.tar.gz'
        `bsdtar -xf "#{archive}" -C #{path}`
      else
        FileUtils.cp(archive, "#{path}/#{archive}")
      end

      list = Find.find(path).reject { |x| File.directory? x }

      list.map do |file|
        Thing.new(file, path, archive)
      end
    end
  end
end
