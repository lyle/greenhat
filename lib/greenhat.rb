#!/usr/bin/env ruby

require 'action_view'
require 'benchmark'
require 'find'
require 'tty-prompt'
require 'tty-spinner'
require 'httparty'
require 'hash_dot'
require 'oj'
require 'slim'
require 'active_support'
require 'active_support/core_ext/array'

Hash.use_dot_syntax = true

# Fix symbol keys
Oj.default_options = { mode: :strict }

require 'greenhat/version.rb'
require 'greenhat/cli.rb'
require 'greenhat/elasticsearch.rb'
require 'greenhat/archive.rb'

# Dashboards
require 'greenhat/kibana.rb'
require 'greenhat/dashboard.rb'

# Thing
require 'greenhat/thing/super_log.rb'
require 'greenhat/thing/info_format.rb'
require 'greenhat/thing/log_format.rb'
require 'greenhat/thing/log.rb'
require 'greenhat/thing.rb'
require 'greenhat/host.rb'

# Name for elasticsearch
# Need to search for container names?
HOST = 'localhost'.freeze
