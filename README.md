# GreenHat

Initial Gem Version / Experimental SOS and Log viewer/parser

12.x and above only (due to structured logging)

## Installation


```ruby
gem install greenhat
```

## Usage

```
greenhat sos-archive.tar.gz
greenhat production_log.json
```

Checkout Kibana - http://localhost:5601
Summary/Info - greenhat.html

![preview](/preview.png)


## Kibana Reference

```
_exists_: field
```

## Development

Local Install

```
be rake install
```

`bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## Testing

```
bundle exec rake
```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/dwalker/greenhat. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GreenHat project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/greenhat/blob/master/CODE_OF_CONDUCT.md).