# lib = File.expand_path('lib', __dir__)
# $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

$LOAD_PATH.push File.expand_path('lib', __dir__)
require 'greenhat/version'

Gem::Specification.new do |spec|
  spec.name          = 'greenhat'
  spec.version       = GreenHat::VERSION
  spec.authors       = ['Davin Walker']
  spec.email         = ['dwalker@gitlab.com']

  spec.summary       = 'Gitlab Test environment in AWS'
  spec.description   = 'Builds out a Gitlab test environment in AWS'
  spec.homepage      = 'https://gitlab.com/dwalker/protobelt'
  spec.license       = 'MIT'

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = ''

    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://gitlab.com/dwalker/greenhat'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{bin,lib}/**/*', 'LICENSE', 'README.md']
  spec.executables   = ['greenhat']

  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.5.0'

  spec.add_development_dependency 'bundler', '~> 1.17'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'pry', '~> 0.12'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rubocop', '~> 0.60.0'
  spec.add_runtime_dependency 'pastel', '~> 0.7'
  spec.add_runtime_dependency 'tty-prompt', '~> 0.17'
  spec.add_runtime_dependency 'tty-spinner', '~> 0.8'

  spec.add_runtime_dependency 'activesupport', '~> 5.2'
  spec.add_runtime_dependency 'actionview', '~> 5.2'
  spec.add_runtime_dependency 'hash_dot', '~> 2.4.1'
  spec.add_runtime_dependency 'httparty', '~> 0.17.0'
  spec.add_runtime_dependency 'oj', '~> 3.7.0'
  spec.add_runtime_dependency 'slim', '~> 4.0.1'
end
